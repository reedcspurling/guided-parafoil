//This file should be included in the parafoil guidance program
//Last verified compatability with parafoil guidance program v.0.9.10

//These are straightforward calculations and other pieces of code which should not require much debugging,
//and have therefore been split off into this file

//------------Libraries to include--------------//
#include <Servo.h> //because we're using a servo motor
#include <TinyGPS++.h> //for communicating with the GPS receiver
//http://arduiniana.org/libraries/tinygpsplus/
#include <SoftwareSerial.h> //For communicating with GPS receiver
//#include <NeoSWSerial.h> //does not work
//https://github.com/SlashDevin/NeoSWSerial
#include <SD.h> //include SD library
#include <SPI.h> //include SPI communication library
#include <Wire.h> //for communicating with the barometric altimeter
#include "IntersemaBaro.h"//code for the barometric altimeter
//https://sites.google.com/site/parallaxinretailstores/home/altimeter-module-ms5607
Intersema::BaroPressure_MS5607B baro(true); //enables code for the barometric altimeter
#include <OneWire.h> //for communicating with the DS18B20 waterproof thermometer
#include <DallasTemperature.h> //for operating the DS18B20 waterproof thermometer
//https://github.com/milesburton/Arduino-Temperature-Control-Library
//#include "UBLOX.h"
//https://github.com/bolderflight/UBLOX



//---------------Variable declarations------------------//
double dlat, dlong; //degrees - differences between target and current latitude/longitude
double currentlatrad; //radians - current position latitude
double targetlatrad; //radians - target position latitude
double adist, cdist; //values for haversine distance calculation
double yhead, xhead; //variables for heading-to-target calculation
double distmag; //meters - great circle distance from current position to target
double desiredheading; //degrees - desired heading to target, calculated at each loop update
int headingdeviation; //degrees - angle between currentheading and desiredheading
const double planetrad = 6371000; //meters - the average radius of whichever planet we're operating on
const double degtorad = (M_PI / 180); //multiply degrees by pi/180 to get radians
const double radtodeg = (180 / M_PI); //multiply radians by 180/pi to get degrees
const int chipselect = BUILTIN_SDCARD; //the teensy 3.5 microcontroller has a built in SD card module. here we state that we will use it
int currentheading; //degrees - the direction that the system is moving in
int currentvelocity; //meters per second
int numberofsats; //how many satellites is the GPS receiver listening to?
int currenthour, currentminute, currentsecond; //current time values as determined by GPS receiver
double currentlat, currentlong, currentalt; //degrees - current latitude of the parafoil
double currentdate; //in DDMMYY format I think
double altscale; //determined by altitude. more altitude = smaller servo movements
double anglescale; //will be determined by magnitude of headingdeviation. (look into PID control for this)
float temperature; //the temperature, as measured by the DS18B20 thermometer
float baroalt; //altitude in meters, measured by the MS5607 barometric altimeter module

double cycle = 0; //incrementing value to keep track of how many times the program has looped
int tempwaitcounter = 0; //counts up by one each cycle until it reaches thermometerdelay, to ensure thermometer has time to think



//function that calculates the direction towards the target
double function1(double targetlat, double currentlat, double targetlong, double currentlong, double degtorad, double radtodeg, double planetrad) {

  //Calculates distmag (great circle distance to target) using haversine formula
  dlat = (targetlat - currentlat) * degtorad; //radians - finds the difference in latitude between system and target
  dlong = (targetlong - currentlong) * degtorad; //radians - finds the difference in longitude between system and target
  currentlatrad = currentlat * degtorad; //radians - current latitude
  targetlatrad = targetlat * degtorad; //radians - current longitude
  adist = sin(dlat / 2) * sin(dlat / 2) + sin(dlong / 2) * sin(dlong / 2) * cos(currentlatrad) * cos(targetlatrad); //angles?...
  cdist = 2 * atan2(sqrt(adist), sqrt(1 - adist)); // I think this uses the arctan2 function to find the angle between two points?
  distmag = planetrad * cdist; //meters - great circle distance from current position to target

  //Calculates desiredheading using results from above
  yhead = sin(dlong) * cos(targetlatrad);
  xhead = cos(currentlatrad) * sin(targetlatrad) - sin(currentlatrad) * cos(targetlatrad) * cos(dlong);
  desiredheading = atan2(yhead, xhead) * radtodeg; //returns desired heading in degrees from -180 to 180
  desiredheading = fmod(desiredheading, 360);
  if (desiredheading < 0)
    desiredheading += 360; //redefines desiredheading
  //the above three lines take degree value of -180 to +180 and turn it into a degree value of 0 to 360

  return desiredheading;
}




//function that calculates the angle between where the parafoil is going and where the parafoil should be going
int function2(double desiredheading, double currentheading) {

  if ((desiredheading - currentheading >= -180) && (desiredheading - currentheading <= 180)) {
    headingdeviation = desiredheading - currentheading;
  }
  else if (desiredheading - currentheading < -180) {
    headingdeviation = 360 + (desiredheading - currentheading);
  }
  else {
    headingdeviation = (desiredheading - currentheading) - 360;
  }

  return headingdeviation;
}




//to do:
//make SD card re-insertable: https://forum.arduino.cc/index.php?topic=156430.0
//neosoftwareserial put it in the standard library and see if it compiles correctly

  //copied instructions for the Lidar-lite module:
/*------------------------------------------------------------------------------
  Connections:
  LIDAR-Lite 5 Vdc (red) to Arduino 5v
  LIDAR-Lite Ground (black) to Arduino GND
  LIDAR-Lite Mode control (yellow) to Arduino digital input (pin 3)
  LIDAR-Lite Mode control (yellow) to 1 kOhm resistor lead 1
  1 kOhm resistor lead 2 to Arduino digital output (pin 2)

  (Capacitor recommended to mitigate inrush current when device is enabled)
  680uF capacitor (+) to Arduino 5v
  680uF capacitor (-) to Arduino GND

  See the Operation Manual for wiring diagrams and more information:
  http://static.garmin.com/pumac/LIDAR_Lite_v3_Operation_Manual_and_Technical_Specifications.pdf
  ------------------------------------------------------------------------------*/

//contact: reedcspurling at gmail dot com